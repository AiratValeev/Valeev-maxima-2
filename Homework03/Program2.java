import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int one = sc.nextInt();
        int two = sc.nextInt();
        int three = sc.nextInt();
        
        while (three != -1) {
            if (one > two && two < three) {
                System.out.println("Local minimum is " + two);
                one = two;
                two = three;
                three = sc.nextInt();
            } else {
                one = two;
                two = three;
            }
        }
        three = sc.nextInt();
    }
}

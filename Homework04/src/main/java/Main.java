import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[121];
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        while (x != -1) {
            array[x] = array[x] +1;
            x = scanner.nextInt();
        }
        int maxAgeCount = 0;
        int indexMax = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxAgeCount) {
                maxAgeCount = array[i];
                indexMax = i;
            }
        }
        System.out.println("Вывод - " + indexMax + " лет");
    }
}

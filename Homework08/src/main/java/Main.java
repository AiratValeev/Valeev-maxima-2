import java.util.Scanner;

public class Main {
    public static int sumRecursion(int x) {
        int res = x % 10;
        if (x < 10) return x;
        res += sumRecursion(x / 10);
        return res;
    }

    public static void main(String[] args) {
        System.out.println("Введите число больше 9");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        System.out.printf("Cумма цифр числа %d = %d", input, sumRecursion(input));
    }
}

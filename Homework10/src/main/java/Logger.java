public class Logger {
    // Статическое поле, которое хранит единственный экземпляр класса.
    private static final Logger instance;

    // Статическая переменная.
    private static String message ;

    // статический инициализатор, который создаёт объект класса.
    static {
        instance = new Logger();
    }

    // Приватный конструктор, запрещаем создание объектов.
    private Logger(){
    }

    // Статический метод для получение единственного экземпляра объекта.
    public static Logger getInstance() {
        return instance;
    }


    public void log(String message) {
        System.out.println(message);
    }
}

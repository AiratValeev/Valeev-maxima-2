/*Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.

Применить паттерн Singleton для Logger.
*/

public class Main {
    public static void main(String[] args) {
        Logger.getInstance().log("Привет");
    }
}


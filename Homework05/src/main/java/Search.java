public class Search {

    public static void selectionSort(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
            /*Сравниваем элементы попарно,
              если они имеют неправильный порядок,
              то меняем местами*/
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
    }

    public static  boolean search(int[] array, int item) {
        int position;

        int first = 0;
        int last = array.length - 1;

        // для начала найдем индекс среднего элемента массива
        position = (first + last) / 2;

        while ((array[position] != item)
                && (first <= last)) {
            if (array[position] > item) {  // если число заданного для поиска
                last = position - 1; // уменьшаем позицию на 1.
            } else {
                first = position + 1;    // иначе увеличиваем на 1
            }
            position = (first + last) / 2;
        }

        return first <= last;
    }
}

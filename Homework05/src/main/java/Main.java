import java.util.Scanner;

/**
 * Реализовать в Program процедуру void selectionSort(int[] array) и
 * функцию boolean search(int[] array) с бинарным поиском.
 */


public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int counter, num, item, array[];
        num = sc.nextInt();
        array = new int[num];
        for( counter = 0; counter <num; counter++){
            array[counter] = sc.nextInt();
        }


        Search.selectionSort(array);
        System.out.println(Search.search(array, 9));
    }
}
